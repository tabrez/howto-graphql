const { prisma } = require('../src/generated/prisma-client');

async function setup() {
  await prisma.createUser({
    name: 'Alice',
    email: 'alice@prisma.io',
    password: 'bobsyouruncle',
    links: {
      create: [
        {
          url: 'www.graphql.org',
          description: 'Query language for APIs'
        },
        {
          url: 'www.prisma.io',
          description: 'Prisma replaces traditional ORM'
        },
        {
          url: 'Github.com',
          description: 'Host for git repositories'
        }
      ]
    }
  });

  await prisma.createUser({
    name: 'Bob',
    email: 'bob@gaphql.org',
    password: 'bobsyouruncle',
    links: {
      create: [
        {
          url: 'www.google.com',
          description: 'Search engine'
        },
        {
          url: 'yahoo.com',
          description: 'news website'
        }
      ]
    }
  });

  await prisma.createUser({
    name: 'R2D2',
    email: 'R2D2@tarwars.com',
    password: 'bobsyouruncle',
    links: {
      create: [
        {
          url: 'python.org',
          description: 'Python programming language'
        },
        {
          url: 'clojure.org',
          description: 'Clojure programming language'
        },
        {
          url: 'nodejs.org',
          description: 'Javascript runtime'
        },
        {
          url: 'javascript.org',
          description: 'Javascript programming language'
        }
      ]
    }
  });
}

setup()
  .then(() => process.exit(0))
  .catch(err => {
    console.error(err);
    process.exit(1);
  });
