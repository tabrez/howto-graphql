FROM node:11.11-alpine

WORKDIR /app/
RUN npm i -g prisma
COPY package.json /app/
RUN npm i

# To run the app in container against Prisma cloud endpoint:
# Copy ~/.prisma/config.yml to current directory to authenticate against
# prisma cloud endpoint, then uncomment below line
# COPY ./config.yml /root/.prisma/config.yml
# Run the container like this:
# source misc/.prisma.env
# docker build -t howto-graphql .
# docker run --rm -p 4000:4000 --name howto-graphql -e PRISMA_URI howto-graphql

COPY prisma /app/prisma
COPY src /app/src

RUN prisma generate

EXPOSE 4000

CMD ["npm", "run", "bootup"]
