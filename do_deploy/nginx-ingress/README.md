# Install Helm, Nginx Ingress, Cert-manager

## Helm

[Install Helm](https://www.digitalocean.com/community/tutorials/how-to-install-software-on-kubernetes-clusters-with-the-helm-package-manager)

## Nginx Ingress

[Install Nginx Ingress](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-with-cert-manager-on-digitalocean-kubernetes)

```bash
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/cloud-generic.yaml
    kubectl apply -f do_deploy/nginx-ingress/howto-graphql-ingress.yml
```

```bash
    kubectl get svc --namespace=ingress-nginx
```

Map external ip address from above command to domain of your choice in DNS
settings

## Cert-manager

[Install Cert-manager using Helm](https://docs.cert-manager.io/en/latest/getting-started/install.html)

```bash
    kubectl create -f do_deploy/nginx-ingress/letsencrypt_issuer.yml
    kubectl apply -f do_deploy/nginx-ingress/howto-graphql-ingress.yml
```

Check the results

```bash
    kubectl describe ingress -n howto-graphql
    kubectl describe certificate -n howto-graphql
```

Open https://howto.seartipy.com/ in the browser and accept risk to open the
webpage.

Repeat above steps to install production certificates.

```bash
    kubectl apply -f do_deploy/nginx-ingress/production_issuer.yml
```

Replace *letsencrypt-staging* with *letsencrypt-prod* in `howto-graphql-ingress.yml`.

```bash
    kubectl apply -f do_deploy/nginx-ingress/howto-graphql-ingress.yml
```
