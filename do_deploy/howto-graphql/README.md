# Create secrets

## Create secret needed to pull howto-graphql docker image from registry.gitlab.com

eval $(cat gitlab.env) && \
    kubectl create secret docker-registry gitlab-registry \
    --docker-server=$DOCKER_REGISTRY_SERVER \
    --docker-username=$DOCKER_USER \
    --docker-password=$DOCKER_PASSWORD \
    --docker-email=$DOCKER_EMAIL  -n howto-graphql

## Create secret to access Prisma service

kubectl create secret generic --from-env-file .credentials.env -n howto-graphql
