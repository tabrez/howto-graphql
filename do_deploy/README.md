# Deploy CRM app to a Kubernetes Cluster

## Install and configure kubectl

[Install with snap on Ubuntu](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-with-snap-on-ubuntu)

```bash
    sudo snap install kubectl --classic
```

Create a cluster on Digital Ocean, download and move the config file to
$HOME/.kube/do-kubeconfig.yaml, configure kubectl to use the config file.

Add the following to your shell profile(.bash_profile, .zshrc, etc.):

```bash
    export KUBECONFIG=~/.kube/config:~/.kube/do-kubeconfig.yaml
```

Look at the contents of *do-kubeconfig.yaml* to get the name of the cluster or
get the cluster name from the output of `kubectl config get-contexts`

```bash
    kubectl config use-context <cluster-name>
```

## Install app in the cluster

Run the following commands from project_folder/do_deploy/ folder.

```bash
    kubectl apply -f namespace.yml

    kubectl create secret generic postgres-credentials \
    --from-env-file postgres/.credentials.env -n howto-graphql
    kubectl apply -f postgres

    kubectl apply -f prisma

    kubectl create secret generic howto-graphql-credentials \
    --from-env-file howto-graphql/.credentials.env -n howto-graphql
    eval $(cat gitlab.env) && \
    kubectl create secret docker-registry gitlab-registry \
    --docker-server=$DOCKER_REGISTRY_SERVER \
    --docker-username=$DOCKER_USER \
    --docker-password=$DOCKER_PASSWORD \
    --docker-email=$DOCKER_EMAIL  -n howto-graphql
    kubectl apply -f howto-graphql
```

## LoadBalancer

Create a load balancer and add all the nodes of Kubernetes cluster to it. Map
the URL you want to the IP address of the load balancer. Go to the URL to access
the app.
