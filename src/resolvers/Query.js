function info() {
  return 'Hello world!';
}

function feed(_parent, _args, context) {
  return context.prisma.links();
}

function links(_parent, _args, context) {
  return context.prisma.links();
}

function link(_parent, args, context) {
  return context.prisma.link({ id: args.id });
}

function users(_parent, _args, context) {
  return context.prisma.users();
}

function user(_parent, args, context) {
  return context.prisma.user({ id: args.id });
}

module.exports = { info, feed, links, users, link, user };
