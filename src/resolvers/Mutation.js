const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const { APP_SECRET, getUserId } = require('../utils');

async function signup(parent, args, context) {
  const password = await bcrypt.hash(args.password, 10);
  const user = await context.prisma.createUser({ ...args, password });
  const token = jwt.sign({ userId: user.id }, APP_SECRET);
  return {
    token,
    user
  };
}

async function login(parent, args, context) {
  const user = await context.prisma.user({ email: args.email });
  if (user) {
    const valid = await bcrypt.compare(args.password, user.password);
    if (!user || !valid) {
      throw new Error('Invalid username or password');
    }
  }
  const token = jwt.sign({ userId: user.id }, APP_SECRET);

  return {
    token,
    user
  };
}

function updateUser(_parent, args, context) {
  return context.prisma.updateUser({
    data: {
      name: args.name,
      email: args.email
    },
    where: { id: args.id }
  });
}

function deleteUser(_parent, args, context) {
  return context.prisma.deleteUser({ id: args.id });
}

function post(_parent, args, context) {
  const userId = getUserId(context);
  const link = {
    description: args.description,
    url: args.url,
    postedBy: { connect: { id: userId } }
  };
  return context.prisma.createLink(link);
}

function updateLink(_parent, args, context) {
  return context.prisma.updateLink({
    data: {
      url: args.url,
      description: args.description
    },
    where: { id: args.id }
  });
}

function deleteLink(_parent, args, context) {
  return context.prisma.deleteLink({ id: args.id });
}

module.exports = {
  signup,
  login,
  updateUser,
  deleteUser,
  post,
  updateLink,
  deleteLink
};
