const gql = require('./server');

const options = { port: 4000 };
gql.server.start(options, () =>
  console.log(`Server is running on http://localhost:${options.port}...`)
);
