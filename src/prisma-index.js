const { prisma } = require('./generated/prisma-client');

async function main() {
  const newLink = await prisma.createLink({
    url: 'www.prisma.io',
    description: 'Prisma replaces traditional ORM'
  });
  console.log(`Created a new link: ${newLink.url} (ID: ${newLink.id})`);

  const allLinks1 = await prisma.links();
  console.log(allLinks1);

  await prisma.deleteLink({ id: newLink.id });
  console.log(`Deleted the link: ${newLink.url} (ID: ${newLink.id})`);

  const allLinks2 = await prisma.links();
  console.log(allLinks2);
}

main().catch(err => console.error(err));
