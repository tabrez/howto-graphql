const supertest = require('supertest');
const assert = require('assert');
const { server } = require('../../src/server');

const query = {
  query: `
    query {
      info
    }`
};

const expected = {
  info: 'Hello world!'
};

function testInfo() {
  // const request = supertest.agent(gql.server.express);
  const request = supertest(server.express);
  console.log('Sending request to /...');
  request
    .post('/')
    .set('Accept', 'application/json')
    .send(query)
    .then(res => {
      // console.log(`response: ${JSON.stringify(res.body.data)}`);
      assert.deepEqual(res.body.data, expected);
      // expect(res.body.data).to.deep.equals(expected);
    })
    .catch(err => {
      console.error(err);
    })
    // why can't we move this to server.finally()?
    .finally(() => {
      console.log('Tests done, now exiting...');
      process.exit(0);
    });
  console.log('Ending tests...');
}

server
  .start({ port: 4001 }, () => {
    console.log('Server is running on http://localhost:4001...');
  })
  .then(() => {
    console.log('Starting tests...');
    testInfo();
  });
