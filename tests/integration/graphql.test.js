const { graphql } = require('graphql');
const assert = require('assert');
const { server } = require('../../src/server');

const query = `
  query {
    info
  }`;

const expected = {
  info: 'Hello world!'
};

graphql(server.executableSchema, query)
  .then(res => {
    assert.deepEqual(res.data, expected);
    console.log('Check completed successfully.');
    console.log(JSON.stringify(res));
  })
  .catch(err => {
    console.error(err);
    process.exit(1);
  });
