const request = require('graphql-request');
const assert = require('assert');
const { server } = require('../../src/server.js');

const query = `
  query {
    info
  }`;

const expected = {
  info: 'Hello world!'
};

server
  .start(() => console.log('Server started...'))
  .then(() => {
    console.log('Sending request...');
    request
      .request('http://localhost:4000/', query)
      .then(data => {
        assert.deepEqual(data, expected);
        console.log('Check completed');
      })
      .catch(error => {
        console.error(error);
        console.error('\nTest failed.');
        process.exit(1);
      })
      .finally(() => {
        console.log('Exiting now...');
        process.exit(0);
      });
  });
